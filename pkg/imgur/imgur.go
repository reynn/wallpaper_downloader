package imgur

type AlbumResult struct {
	Data    []AlbumData `json:"data"`
	Success bool        `json:"success"`
	Status  int64       `json:"status"`
}

type AlbumImagesResult struct {
	Data    []Image `json:"data"`
	Success bool    `json:"success"`
	Status  int64   `json:"status"`
}

type AlbumData struct {
	ID          string  `json:"id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Datetime    int64   `json:"datetime"`
	Cover       string  `json:"cover"`
	AccountURL  string  `json:"account_url"`
	AccountID   int64   `json:"account_id"`
	Privacy     string  `json:"privacy"`
	Layout      string  `json:"layout"`
	Views       int64   `json:"views"`
	Link        string  `json:"link"`
	ImagesCount int64   `json:"images_count"`
	Images      []Image `json:"images"`
}

type Image struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Datetime    int64  `json:"datetime"`
	Type        string `json:"type"`
	Animated    bool   `json:"animated"`
	Width       int64  `json:"width"`
	Height      int64  `json:"height"`
	Size        int64  `json:"size"`
	Views       int64  `json:"views"`
	Bandwidth   int64  `json:"bandwidth"`
	Link        string `json:"link"`
}
