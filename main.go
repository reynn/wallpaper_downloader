package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"

	"github.com/gocolly/colly"
	"github.com/reynn/wallpaper_downloader/pkg/imgur"
)

var (
	jsonPath string
)

type handlerFunc func(a *Album) error

type Album struct {
	URL   string
	Title string
}

func (a *Album) GetSite() (string, error) {
	if u, e := url.Parse(a.URL); e != nil {
		return "", e
	} else {
		return u.Host, nil
	}
}

func (a *Album) Download(f handlerFunc) {
	if e := f(a); e != nil {
		log.Fatal(e)
	}
}

type Downloadable interface {
	download() error
}

type Download struct {
	URL         string
	OutDir      string
	OutFileName string
}

func (d *Download) download() error {
	if resp, e := http.Get(d.URL); e != nil {
		return e
	} else {
		defer resp.Body.Close()
		if b, e := ioutil.ReadAll(resp.Body); e != nil {
			return e
		} else {
			if e := os.MkdirAll(d.OutDir, os.ModePerm); e != nil {
				return e
			} else {
				if d.OutFileName == "" {
					splitUrl := strings.Split(d.URL, "/")
					d.OutFileName = splitUrl[len(splitUrl)-1]
				}
				outFileName := path.Join(d.OutDir, d.OutFileName)
				log.Printf("Saving %s.", outFileName)
				if f, e := os.Stat(outFileName); e == nil {
					if f.Size() != int64(len(b)) {
						if e := ioutil.WriteFile(outFileName, b, os.ModePerm); e != nil {
							return e
						}
					}
				} else {
					if e := ioutil.WriteFile(outFileName, b, os.ModePerm); e != nil {
						return e
					}
				}
			}
		}

	}
	return nil
}

type Processor struct {
	URL               string
	HTMLQuerySelector string
	OnHTMLFunc        func(e *colly.HTMLElement)
	Headers           map[string]string
}

func (p *Processor) Work() {
	c := colly.NewCollector(
		colly.UserAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"),
		colly.CacheDir("cache"),
	)

	c.OnHTML(p.HTMLQuerySelector, p.OnHTMLFunc)

	if len(p.Headers) > 0 {
		c.OnRequest(func(request *colly.Request) {
			for k, v := range p.Headers {
				request.Headers.Set(k, v)
			}
		})
	}

	c.OnRequest(func(r *colly.Request) {
		fmt.Printf("Visiting %s\n", r.URL)
	})

	if e := c.Visit(p.URL); e != nil {
		log.Fatalf("Failed to visit site: %v", e)
	}
}

func getContentFromUrl(u string) ([]byte, error) {
	cacheFileName := filepath.Join("cache",
		strings.Replace(
			strings.Replace(
				strings.Replace(
					strings.Replace(
						strings.Replace(strings.Replace(u, "http://", "", -1), "https://", "", -1),
						"&", "_", -1),
					";", "_", -1),
				"/", "_", -1),
			"?", "_", -1))
	if _, e := os.Stat(cacheFileName); e == nil {
		return ioutil.ReadFile(cacheFileName)
	} else {
		client := http.Client{}
		req, e := http.NewRequest("GET", u, nil)
		if e != nil {
			return nil, e
		}
		if strings.Contains(u, "imgur.com") {
			req.Header.Add("Authorization", fmt.Sprintf("Client-ID %s", os.Getenv("IMGUR_CLIENT_ID")))
		} else {
			req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")
		}
		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}
		if resp.StatusCode == http.StatusNotFound {
			return nil, fmt.Errorf("failed to find gallery: %s", u)
		}
		defer resp.Body.Close()
		if b, e := ioutil.ReadAll(resp.Body); e != nil {
			return nil, e
		} else {
			if e := ioutil.WriteFile(cacheFileName, b, os.ModePerm); e != nil {
				log.Fatal(e)
			} else {
				return b, nil
			}
		}
	}
	return nil, nil
}

func handleImgur(a *Album) error {
	splitUrl := strings.Split(a.URL, "/")
	albumID := splitUrl[len(splitUrl)-1]
	imgurApiUrl := "https://api.imgur.com/3/album/" + albumID + "/images"

	if content, err := getContentFromUrl(imgurApiUrl); err != nil {
		return err
	} else {
		var album imgur.AlbumImagesResult
		if e := json.Unmarshal(content, &album); e != nil {
			log.Printf("JSON: %s", string(content))
			log.Fatalf("Failed to unmarshal Imgur JSON: %v", e)
		}
		wg := sync.WaitGroup{}
		wg.Add(len(album.Data))
		log.Printf("Downloading %d images from Imgur album: %s", len(album.Data), albumID)
		for _, img := range album.Data {
			go func(imageLink string) {
				defer wg.Done()
				d := &Download{
					URL:    imageLink,
					OutDir: path.Join("out", a.Title),
				}
				if e := d.download(); e != nil {
					log.Printf("Error downloading image: %v", e)
				}
			}(img.Link)
		}
		wg.Wait()
	}
	return nil
}
func handleAlphaCoder(a *Album) error {
	wg := sync.WaitGroup{}

	p := &Processor{
		URL: a.URL,
		OnHTMLFunc: func(e *colly.HTMLElement) {
			link := e.Attr("data-href")
			splitUrl := strings.Split(link, "/")
			ext := splitUrl[len(splitUrl)-2]
			file := strings.Join(splitUrl[len(splitUrl)-4:], "_")
			d := &Download{
				URL:         link,
				OutDir:      path.Join("out", a.Title),
				OutFileName: fmt.Sprintf("%s.%s", file, ext),
			}
			wg.Add(1)
			go func(down *Download) {
				defer wg.Done()
				if e := down.download(); e != nil {
					log.Printf("Failed to download %s: %v", down.URL, e)
				}
			}(d)
		},
		HTMLQuerySelector: ".download-button",
	}
	p.Work()

	wg.Wait()

	p = &Processor{
		URL:               a.URL,
		HTMLQuerySelector: ".hidden-xs.hidden-sm ul.pagination li:nth-last-child(1)",
		OnHTMLFunc: func(e *colly.HTMLElement) {
			if !strings.Contains(e.Attr("class"), "active") {
				link := e.ChildAttr("a", "href")
				handleAlphaCoder(&Album{
					Title: a.Title,
					URL:   e.Request.AbsoluteURL(link),
				})
			}
		},
	}
	p.Work()
	return nil
}
func handleWallpaperscraft(a *Album) error {
	log.Printf("Downloading %s album from Wallpapers Craft", a.Title)
	return nil
}
func handleForgottenLair(a *Album) error {
	log.Printf("Downloading %s album from Forgotten Lair", a.Title)
	p := &Processor{
		URL:               a.URL,
		HTMLQuerySelector: ".set-target",
		OnHTMLFunc: func(e *colly.HTMLElement) {
			link := e.Attr("href")
			log.Printf("ForgottenLair link [%s]", link)
		},
	}
	p.Work()
	return nil
}
func handleZerochan(a *Album) error {
	wg := sync.WaitGroup{}
	p := &Processor{
		URL: a.URL,
		OnHTMLFunc: func(e *colly.HTMLElement) {
			link := e.Attr("href")
			if strings.HasPrefix(link, "https://static.zerochan.net") {
				d := &Download{
					URL:    link,
					OutDir: path.Join("out", a.Title, ""),
				}
				wg.Add(1)
				go func(down *Download) {
					defer wg.Done()
					if e := down.download(); e != nil {
						log.Printf("Error downloading image: %v", e)
					}
				}(d)
			}
		},
		HTMLQuerySelector: "#thumbs2 li p a",
	}
	p.Work()
	wg.Wait()
	return nil
}
func handleWallpaperCave(a *Album) error {
	wg := sync.WaitGroup{}
	p := &Processor{
		URL: a.URL,
		OnHTMLFunc: func(e *colly.HTMLElement) {
			link := e.ChildAttr("img", "src")
			d := &Download{
				URL:    e.Request.AbsoluteURL(link),
				OutDir: path.Join("out", a.Title, ""),
			}
			wg.Add(1)
			go func(down *Download) {
				defer wg.Done()
				if e := down.download(); e != nil {
					log.Printf("Error downloading image: %v", e)
				}
			}(d)
		},
		HTMLQuerySelector: ".wpinkw",
	}
	p.Work()
	wg.Wait()
	return nil
}

func init() {
	flag.StringVar(&jsonPath, "json-path", "", "Path to the JSON containing data for the album downloads.")
	flag.Parse()
}

func main() {
	if jsonPath == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	// ensure cache dir exists
	if _, e := os.Stat("cache"); e != nil {
		log.Printf("Creating cache directory")
		os.Mkdir("cache", os.ModePerm)
	}

	mapper := map[string]handlerFunc{
		"imgur.com":                handleImgur,
		"wall.alphacoders.com":     handleAlphaCoder,
		"wallpapercave.com":        handleWallpaperCave,
		"www.zerochan.net":         handleZerochan,
		"www.theforgottenlair.net": handleForgottenLair,
		"wallpaperscraft.com":      handleWallpaperscraft,
	}

	b, err := ioutil.ReadFile(jsonPath)
	if err != nil {
		log.Fatalf("Error reading file: %v", err)
	}

	var albums []Album
	if err := json.Unmarshal(b, &albums); err != nil {
		log.Fatalf("Failed to parse JSON: %v", err)
	}

	wg := sync.WaitGroup{}
	for _, album := range albums {
		if h, e := album.GetSite(); e != nil {
			log.Fatalf("Failed to determine host for %s [%v]", album.URL, e)
		} else {
			f := mapper[h]
			if f != nil {
				wg.Add(1)
				go func(a Album, h handlerFunc) {
					defer wg.Done()
					a.Download(h)
				}(album, f)
			} else {
				log.Printf("Unable to determine how to handle: %s", h)
			}
		}
	}
	wg.Wait()
}
