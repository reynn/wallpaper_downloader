FROM golang:1.10.3-alpine3.7 AS builder

COPY . /go/src/github.com/reynn/wallpaper_downloader/

WORKDIR /go/src/github.com/reynn/wallpaper_downloader/

RUN apk add --update --no-cache git ca-certificates \
    && go get -u github.com/golang/dep/cmd/dep \
    && dep ensure \
    && go build -o /bin/wallpaper_downloader

FROM alpine:3.8

COPY --from=builder /bin/wallpaper_downloader /usr/local/bin/wallpaper_downloader

WORKDIR /tmp

ENTRYPOINT [ "wallpaper_downloader" ]