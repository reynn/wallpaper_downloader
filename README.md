# Wallpaper Downloader GO

I created this to do mass downloads of images from wallpaper sites or galleries on Imgur. Any album on Imgur could be downloaded this way and support for other sites could be added from here.

## Usage

Create a JSON file with the data for the albums to download.

```json
[
    {
        "url": "https://wall.alphacoders.com/by_sub_category.php?id=240912&amp;name=My+Hero+Academia+Wallpapers",
        "title": "Boku no Hero Academia"
    }
]
```

The `url` should point to the site to download from, the `title` will become the folder all images are put into.

Once the JSON is created you can run this script using:

```shell
go get -u github.com/golang/dep/cmd/dep
dep ensure
go run main.go -json-path albums.json
```